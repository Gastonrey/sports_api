# Introduction

## Table of content
  * [Description](#description)
  * [Docker](#docker)
  * [Pending Implementations](#pending-implementations-and-improvements)
  * [Seed and Start Up](#seed-and-start-up)

## Description

This project is a REST API that contains two endpoints:

Search by league and season:

`GET  /api/results/search  query params: league & season`

Retrieve all results:

`GET  /api/results`

This project provides a MIX task located at `lib/mix/tasks` which allows users to import all the results from a given CSV. This being said, all the models and solution is specific for this kind of CSV template.

By running `mix help` it can be found the mix task as follow:

`mix derivco_sports_api.import_csv # Given a CSV file path, populates the database`

## Docker

This project has been containerized. See bellow how to start it up by docker-compose and swarm.

## Pending implementations and improvements

There were some stuff that I couldn't achieve so far:

 * Swagger: Tried to add it but had some problemas with last version of Phoenix (didn't have to do that before in Elixir). Otherwise used Ex Docs to document
 * Didn't have time to add Telemetry or any other monitorization, would be grate to do so
 * Protocols Buffer: Didn't add this support since has never worked with it, so focused just on REST
 * Kubernetes: Didn't have time to achieve it
 * Pagination: Endpoints should use any pagination before rendering results to user. I use to use Scrivener, but didn't have time to implemnt it here

## Seed and start up:

Before anything else install dependencies with `mix deps.get`.

As described above this is just an API, the idea is to use the provided mix task to import a match results CSV and use its API to query these results.

### Seeding

the mix task supports one parameter which is the path to CSV file, and if not parameter is provided then this will look for the CSV file on the actual path.

*IMPORTANT*: This API is backed by a PostgreSql database, so the mix task will first perform a `mix ecto.setup` in order to create the DB. Please, be sure you have the correct configuration at `config/{Mix.env}.exs`. If you are trying to import this data into a docker running service, be sure it's been already started (See bellow on how to start it by Docker) and be sure you DB configuration points to it host and port.

Now run: `mix derivco_sports_api.import_csv -f path/to/csv file`

These should be enough and when result is :ok a message will be displayed or the exception instead.


### Start up

This app can be started just as a Phoenix project `mix phx.server` or you can use swarm in order to deploy the stack and get running 3 services, the DB and the HA proxy.

To start up all the stack it requires:

 - Initialize Swarm: `docker swarm init`
 - A network to already exist, look at the docker-compose.yml file to see the expected network's name. If it's not created already you can run: `docker network create --driver overlay web` where `web` is the network's name.
 - Finally deploy the stack: `docker stack deploy --compose-file=docker-compose.yml prod` where `prod` is the stack's name.

If all this worked as expected, running the command `docker service ls` we should see the 3 services + the HA proxy already up and running, as well as their exposed ports.

## Docs

This projects uses `ex_docs`, so run `mix docs` if you would like to see html documented modules.
