FROM bitwalker/alpine-elixir:1.8.2

ENV DEBIAN_FRONTEND=noninteractive

# Install hex
RUN mix local.hex --force
RUN mix local.rebar --force

ENV APP_HOME /app

WORKDIR $APP_HOME

COPY . .

CMD mix do deps.get, phx.server