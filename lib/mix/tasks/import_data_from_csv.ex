defmodule Mix.Tasks.DerivcoSportsApi.ImportCsv do
  use Mix.Task

  require DerivcoSportsApiWeb.CsvProcessor, as: CsvProcessor

  @shortdoc "Given a CSV file path, populates the database"

  @switches [file: :string]

  @aliases [f: :file]

  @moduledoc """
  This task populates a DB with all the records
  in the given csv file.

  ## Use Examples

      mix derivco_sports_api.import_csv # Asumes CSV file is at actual path
      mix derivco_sports_api.import_csv -f /path/to/json_file

  ## Command line options

    * `-f`, `--file` - the path to the CSV file
  """

  @doc false
  def run(args) do
    Application.ensure_all_started(:derivco_sports_api)

    {opts, _} = OptionParser.parse!(args, strict: @switches, aliases: @aliases)

    file_path = opts[:file] || Path.wildcard("./*.csv")

    Mix.shell().info("Setup database before importing...")

    with 0 <- Mix.shell().cmd("mix ecto.setup"),
         :ok <- CsvProcessor.parse_and_insert(file_path) do
      Mix.shell().info("Import has finished successfuly")
    end
  rescue
    _e in ArgumentError ->
      Mix.shell().error("Error parsing file...")
  end
end
