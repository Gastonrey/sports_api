defmodule DerivcoSportsApiWeb.Model.MatchResult do
  use Ecto.Schema
  import Ecto.Changeset

  alias DerivcoSportsApiWeb.Model.Season

  @derive {Poison.Encoder, except: [:__meta__, :id, :inserted_at, :updated_at, :season_id, :season]}
  schema "match_results" do
    field(:date, :utc_datetime)
    field(:full_time_home_team_goals, :string)
    field(:full_time_away_team_goals, :string)
    field(:full_time_result, :string)
    field(:half_time_home_team_goals, :string)
    field(:half_time_away_team_goals, :string)
    field(:half_time_result, :string)
    field(:home_team, :string)
    field(:away_team, :string)

    belongs_to(:season, Season)

    timestamps()
  end

  def printable_fields do
    [
      :date,
      :full_time_result,
      :full_time_away_team_goals,
      :full_time_home_team_goals,
      :half_time_result,
      :half_time_away_team_goals,
      :half_time_home_team_goals,
      :home_team,
      :away_team
    ]
  end

  @doc false
  def changeset(match, attrs \\ %{}) do
    match
    |> cast(attrs, [])
    |> validate_required([])
  end
end
