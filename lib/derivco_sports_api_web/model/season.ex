defmodule DerivcoSportsApiWeb.Model.Season do
  use Ecto.Schema
  import Ecto.Changeset

  alias DerivcoSportsApiWeb.Model.League
  alias DerivcoSportsApiWeb.Model.MatchResult
  alias DerivcoSportsApi.Repo

  @derive {Poison.Encoder, except: [:__meta__]}
  schema "seasons" do
    field(:name, :string)

    belongs_to(:league, League)
    has_many(:match_results, MatchResult, [{:on_delete, :delete_all}, {:on_replace, :nilify}])

    timestamps()
  end

  @doc false
  def changeset(season, attrs) do
    season
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def add_match(season, attrs) do
    season = season |> Repo.preload(:match_results)

    season
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:match_results, [
      struct(%MatchResult{}, attrs) | season.match_results
    ])
    |> Repo.update!()
  end
end
