defmodule DerivcoSportsApiWeb.Model.Queries do
  import Ecto.Query, warn: false

  alias DerivcoSportsApi.Repo
  alias DerivcoSportsApiWeb.Model.Season
  alias DerivcoSportsApiWeb.Model.League
  alias DerivcoSportsApiWeb.Model.MatchResult

  def get_results_by_league_and_season(league, season) do
    result =
      from(l in League,
        where: l.name == ^league,
        join: s in assoc(l, :seasons),
        where: s.name == ^season,
        select: struct(s, [:id])
      )
      |> Repo.all()
      |> Repo.preload(:match_results)

    %{
      league: league,
      season: season,
      results:
        Enum.reduce(result, [], fn x, acc ->
          [x.match_results |> List.first() | acc]
        end)
    }
  end

  def get_results do
    from(l in League,
      join: s in assoc(l, :seasons),
      where: s.league_id == l.id,
      join: m in assoc(s, :match_results),
      select: %{
        results: struct(m, ^MatchResult.printable_fields()),
        league: l.name,
        season: s.name
      },
      group_by: [s.id, m.id, l.id]
    )
    |> Repo.all()
    |> Enum.group_by(& &1.league)
    |> Enum.map(fn {key, value} ->
      %{
        league: key,
        results: extract_results_from_struct(value),
        seasons: extract_season_from_struct(value)
      }
    end)
  end

  defp extract_results_from_struct(values) do
    values
    |> Enum.map(
      &(Map.from_struct(&1.results)
        |> Map.take(MatchResult.printable_fields())
        )
    )
    |> Enum.uniq()
  end

  defp extract_season_from_struct(values) do
    values
    |> Enum.map(& &1.season)
    |> Enum.uniq()
  end
end
