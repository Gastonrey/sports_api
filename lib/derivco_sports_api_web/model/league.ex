defmodule DerivcoSportsApiWeb.Model.League do
  use Ecto.Schema
  import Ecto.Changeset

  alias DerivcoSportsApi.Repo
  alias DerivcoSportsApiWeb.Model.Season

  @derive {Poison.Encoder, except: [:__meta__]}
  schema "leagues" do
    field(:name, :string)

    has_many(:seasons, Season, [{:on_delete, :delete_all}, {:on_replace, :nilify}])

    timestamps()
  end

  @doc false
  def changeset(league, attrs) do
    league
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def create(attrs) do
    Repo.insert!(struct(__MODULE__, attrs))
  end

  def add_season(league, attrs \\ %{}) do
    league = league |> Repo.preload(:seasons)

    league
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:seasons, [struct(%Season{}, attrs) | league.seasons])
    |> Repo.update!()
  end
end
