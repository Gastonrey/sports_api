defmodule DerivcoSportsApiWeb.CsvProcessor do
  alias DerivcoSportsApiWeb.Model.League
  alias DerivcoSportsApiWeb.Model.Season
  alias DerivcoSportsApi.Repo
  alias Flow.Window, as: FlowWindow

  require Logger

  @moduledoc "
    This module uses [Flow](https://hexdocs.pm/flow/Flow.html) to parse and process large CSV files.
  "

  @doc """
    Entry point to parse and insert the csv on DB.

    iex> DerivcoSportsApiWeb.CsvProcessor.parse_and_insert(./test.csv)
  """
  def parse_and_insert(file) do
    start_parsing_file(file)
    |> Flow.each(fn entry ->
      Repo.transaction(fn ->
        league =
          League.create(%{name: entry["Div"]})
          |> League.add_season(%{name: entry["Season"]})

        league.seasons
        |> List.first()
        |> Season.add_match(%{
          date: convert_string_date(entry["Date"]),
          full_time_home_team_goals: entry["FTHG"],
          full_time_away_team_goals: entry["FTAG"],
          full_time_result: entry["FTR"],
          half_time_home_team_goals: entry["HTHG"],
          half_time_away_team_goals: entry["HTAG"],
          half_time_result: entry["HTR"],
          home_team: entry["HomeTeam"],
          away_team: entry["AwayTeam"]
        })
      end)
    end)
    |> Flow.run()
  catch
    :exit, _ ->
      Logger.error(
        "There was an error trying to import CSV file. Database is going to be dropped"
      )

      %{error: "Dates are wrong, it should be in format DD-MM-YYYY"}
  end

  # Looks for the csv file and starts parsing it.
  # Returns a partitioned flow.
  defp start_parsing_file(file) do
    File.stream!(file, read_ahead: 10_00)
    |> CSV.decode!(separator: ?,, headers: true)
    |> Flow.from_enumerable()
    |> Flow.filter(fn row ->
      row["FTHG"] != "" && row["FTAG"] != ""
    end)
    |> split_in_batches()
  end

  defp convert_string_date(date_str) do
    # Set date as y-m-d
    [_match, d, m, y] = Regex.run(~r/^([0-9]+)\/([0-9]+)\/([0-9]+)$/, date_str)

    # Zero offset
    ("#{y}-#{m}-#{d}" <> " 00:00:00Z")
    |> Timex.parse!("{ISO:Extended}")
  end

  defp split_in_batches(input_flow) do
    input_flow
    |> Flow.partition(window: FlowWindow.count(1_000), stages: 4)
    |> Flow.reduce(fn -> [] end, fn item, batch ->
      [item | batch]
    end)
  end
end
