defmodule DerivcoSportsApiWeb.ResultsController do
  use DerivcoSportsApiWeb, :controller

  alias DerivcoSportsApiWeb.Model.Queries

  @doc """
  Retrieves all the match results matching the given league and season query Parameters

  ## Parameters

    - league: String
    - season: String

  ## Examples

      /api/results/search?league=SP1&season=201617

  """
  def search(conn, %{"league" => league, "season" => season}) do
    csv_results = Queries.get_results_by_league_and_season(league, season)
    render(conn, "show.json", results: csv_results)
  end

  @doc """
  Retrieves all the match results by league and season

  ## Examples

      /api/results

  """
  def index(conn, %{}) do
    csv_results = Queries.get_results()
    render(conn, "index.json", results: csv_results)
  end
end
