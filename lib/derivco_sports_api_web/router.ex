defmodule DerivcoSportsApiWeb.Router do
  use DerivcoSportsApiWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", DerivcoSportsApiWeb do
    pipe_through(:api)

    get "/results/search", ResultsController, :search
    resources("/results", ResultsController, only: [:index])
  end
end
