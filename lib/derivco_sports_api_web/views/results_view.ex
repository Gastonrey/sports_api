defmodule DerivcoSportsApiWeb.ResultsView do
  use DerivcoSportsApiWeb, :view

  def render("index.json", %{results: res}) do
    res
  end

  def render("show.json", %{results: res}) do
    res
  end

  def render("index.json", _) do
    render(DerivcoSportsApiWeb.ErrorView, "400.json")
  end

  def render("show.json", _) do
    render(DerivcoSportsApiWeb.ErrorView, "400.json")
  end
end
