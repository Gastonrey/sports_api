defmodule DerivcoSportsApi.Repo.Migrations.AddReferenciesForDeletion do
  use Ecto.Migration

  def change do
    execute "ALTER TABLE seasons DROP CONSTRAINT seasons_league_id_fkey"
    alter table(:seasons) do
      modify :league_id, references(:leagues, on_delete: :delete_all)
    end

    execute "ALTER TABLE match_results DROP CONSTRAINT match_results_season_id_fkey"
    alter table(:match_results) do
      modify :season_id, references(:seasons, on_delete: :delete_all)
    end
  end
end
