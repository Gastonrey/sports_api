defmodule DerivcoSportsApi.Repo.Migrations.CreateSeasons do
  use Ecto.Migration

  def change do
    create table(:seasons) do
      add :name, :string
      add :league_id, references(:leagues)

      timestamps()
    end

  end
end
