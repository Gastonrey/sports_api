defmodule DerivcoSportsApi.Repo.Migrations.CreateMatches do
  use Ecto.Migration

  def change do
    create table(:match_results) do
      add :date, :utc_datetime
      add :full_time_home_team_goals, :string
      add :full_time_away_team_goals, :string
      add :full_time_result, :string
      add :half_time_home_team_goals, :string
      add :half_time_away_team_goals, :string
      add :half_time_result, :string
      add :home_team, :string
      add :away_team, :string
      add :season_id, references(:seasons)

      timestamps()
    end

  end
end
