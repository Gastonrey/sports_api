# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :derivco_sports_api,
  ecto_repos: [DerivcoSportsApi.Repo]

# Configures the endpoint
config :derivco_sports_api, DerivcoSportsApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "dkPmLGEKi3cyborgCng/LDrnQ+vA9E9dboAuRAdesVmWXcXFUixhpmsT6JyeTSn9",
  render_errors: [view: DerivcoSportsApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DerivcoSportsApi.PubSub, adapter: Phoenix.PubSub.PG2]

config :phoenix, :json_library, Poison

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :logger,
  backends: [:console, {LoggerFileBackend, :error_log}],
  format: "[$level] $message\n"

config :logger, :error_log,
  path: "/tmp/error_events_app.log",
  level: :error

config :logger, :info_log,
  path: "/tmp/info_events_app.log",
  level: :info

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"