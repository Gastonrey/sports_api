defmodule DerivcoSportsApiWeb.Factory do
  alias DerivcoSportsApi.Repo
  alias DerivcoSportsApiWeb.Model.Season
  alias DerivcoSportsApiWeb.Model.League
  alias DerivcoSportsApiWeb.Model.MatchResult

  # @match_attrs %{
  #   date: "",
  #   home_team: "",
  #   away_team: "",
  #   full_time_home_team_goals: "1",
  #   full_time_away_team_goals: "2",
  #   full_time_result: "0",
  #   half_time_home_team_goals: "0",
  #   half_time_away_team_goals: "0",
  #   half_time_result: "0"
  # }

  def build(:league) do
    %League{name: "League foo"}
  end

  def build(:season) do
    %Season{name: "201617"}
  end

  def build(:match_results) do
    season = build(:season)

    Season.add_match(season, %{home_team: "foo"})
  end

  # Convenience API

  def build(factory_name, attributes) do
    factory_name |> build() |> struct(attributes)
  end

  def insert!(factory_name, attributes \\ []) do
    Repo.insert!(build(factory_name, attributes))
  end
end
