defmodule DerivcoSportsApiWeb.ViewTest do
  use DerivcoSportsApiWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders index.json" do
    assert render(DerivcoSportsApiWeb.ResultsView, "index.json", %{results: %{foo: "bar"}}) ==
             %{foo: "bar"}
  end

  test "renders show.json" do
    assert render(DerivcoSportsApiWeb.ResultsView, "show.json", %{results: %{foo: "bar"}}) ==
             %{foo: "bar"}
  end

  test "renders a wrong result" do
    assert render(DerivcoSportsApiWeb.ResultsView, "show.json", %{foo: "bar"}) ==
             %{errors: %{detail: "Not found"}}
  end
end
