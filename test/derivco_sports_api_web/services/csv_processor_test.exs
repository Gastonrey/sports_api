defmodule DerivcoSportsApiWeb.CsvProcessorTest do
  use DerivcoSportsApiWeb.ConnCase

  alias DerivcoSportsApiWeb.CsvProcessor
  alias DerivcoSportsApiWeb.Model.Queries
  alias DerivcoSportsApiWeb.Model.League
  alias DerivcoSportsApi.Repo

  defp clean_up(_) do
    Repo.delete_all(League)

    :ok
  end

  def expected_result do
    date = DateTime.from_iso8601("2016-08-19 00:00:00Z") |> elem(1)

    [
      %{
        away_team: "Osasuna",
        date: date,
        full_time_away_team_goals: "1",
        full_time_home_team_goals: "1",
        full_time_result: "D",
        half_time_away_team_goals: "0",
        half_time_home_team_goals: "0",
        half_time_result: "D",
        home_team: "Malaga"
      },
      %{
        away_team: "Eibar",
        date: date,
        full_time_away_team_goals: "1",
        full_time_home_team_goals: "2",
        full_time_result: "H",
        half_time_away_team_goals: "0",
        half_time_home_team_goals: "0",
        half_time_result: "D",
        home_team: "La Coruna"
      }
    ]
  end

  setup_all do
    on_exit(fn ->
      clean_up([])
    end)

    :ok
  end

  describe "parse_and_insert/1 successfuly" do
    setup [:clean_up]

    test "correct input gets parsed and imported" do
      CsvProcessor.parse_and_insert("./test/fixtures/correct_csv.csv")
      [head | tail] = Queries.get_results()
      %{league: _, results: res} = head

      assert %{league: "SP1", results: _} = head
      assert expected_result = res
    end
  end

  describe "parse_and_insert/1 wrong input dates" do
    setup [:clean_up]

    test "trying to import a csv with wrong dates format should stop the process" do
      result = CsvProcessor.parse_and_insert("./test/fixtures/csv_with_wrong_date_format.csv")

      assert %{error: "Dates are wrong, it should be in format DD-MM-YYYY"} = result
      assert [] = Queries.get_results()
    end
  end
end
