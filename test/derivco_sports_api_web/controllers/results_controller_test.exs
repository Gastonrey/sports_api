defmodule DerivcoSportsApiWeb.ResultsControllerTest do
  use DerivcoSportsApiWeb.ConnCase
  alias DerivcoSportsApiWeb.Factory
  alias DerivcoSportsApiWeb.Model.League
  alias DerivcoSportsApi.Repo

  defp seed_with_results(_) do
    for sequence <- Enum.to_list(1..5) do
      league = Factory.insert!(:league, %{name: "foo_#{sequence}"})

      Factory.insert!(:season, %{
        name: Integer.to_string(sequence),
        league_id: league.id,
        match_results: [%{home_team: "floo", away_team: "rio"}]
      })
    end

    {:ok, match_result: :ok}
  end

  defp clean_up(_) do
    Repo.delete_all(League)

    :ok
  end

  setup_all do
    seed_with_results([])

    on_exit(fn ->
      clean_up([])
    end)

    :ok
  end

  describe "index/2" do
    test "it filters by the given params", %{conn: conn} do
      response =
        conn
        |> get(results_path(conn, :search, %{league: "foo_1", season: "1"}))
        |> json_response(200)

      assert %{"league" => "foo_1", "season" => "1"} = response

      results = response
      |> Map.get("results")
      |> List.first

      assert %{"home_team" => "floo", "away_team" => "rio"} = results
    end
  end

  describe "index/0" do
    test "given all the leagues called #foo, returns just one key grouping all results", %{
      conn: conn
    } do
      response =
        conn
        |> get(results_path(conn, :index, %{}))
        |> json_response(200)

      league_results = response |> List.first

      assert 5 = Kernel.length(response)
      assert is_list(league_results["seasons"])
      assert is_list(league_results["results"])
      assert is_binary(league_results["league"])
    end
  end
end
